// Carson Gedeus
// COP3402, PL/0 (Pascal Language) Lexical Analyer

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <ctype.h>
#include <string.h>

typedef struct node
{
	char *word;
	struct node *next;
} node;

typedef enum			//Given Token Types
{
	nulsym = 1, identsym = 2, numbersym = 3, plussym = 4, minussym = 5, multsym = 6, slashsym = 7, oddsym = 8,
	eqlsym = 9, neqsym = 10, lessym = 11, leqsym = 12,
	gtrsym = 13, geqsym = 14, lparentsym = 15, rparentsym = 16, commasym = 17, semicolonsym = 18, periodsym = 19,
	becomessym = 20, beginsym = 21, endsym = 22, ifsym = 23, thensym = 24, whilesym = 25, dosym = 26, callsym = 27,
	constsym = 28, varsym = 29, procsym = 30, writesym = 31, readsym = 32, elsesym = 33
} token_type;

node* starts_with_letter(FILE* inputFile, node* tail, char firstLetter);
node* starts_with_integer(FILE* inputFile, node* tail, char firstDigit);
node* init_symbol(FILE* inputFile, node* tail, char firstSymbol);
void findLexeme(char *text, int *numSymbs);
node* createNode();

// Global flags set by command line argument
int source = 0;
int clean = 0;

int main(int argc, char * argv[])
{
	int c = 0, symbol_int = 0;
	char letter;

	node *head, *tail;
	head  = tail = createNode();

	FILE *inputFilePointer = fopen("file.txt", "r");
	if( inputFilePointer == NULL)
	{
		printf("\"file.txt\" file not found\n");
		exit(1);
	}
	if(argc > 2)
	{
		if(strcmp(argv[2],"--clean") == 0)
		{
			printf("source code without comments:\n");
			printf("-----------------------------\n");
			
			clean = 1;
		}
		if(strcmp(argv[2],"--source") == 0)
		{
			
			source = 1;
		}
	}
	
	if(argc > 3)
	{
		if(strcmp(argv[3],"--clean") == 0)
		{
			printf("source code without comments:\n");
			printf("-----------------------------\n");
			clean = 1;
		}
		if(strcmp(argv[3],"--source") == 0)
		{
			source = 1;
		}
	}

	
	c = fgetc(inputFilePointer);
	while( c != EOF)
	{
		
		if( isalpha(c) )
		{
			letter = c;
			tail = starts_with_letter(inputFilePointer, tail, c);
			c = fgetc(inputFilePointer);
		}
		else if( isdigit(c))
		{
			tail = starts_with_integer(inputFilePointer, tail, c);
			c = fgetc(inputFilePointer);
		}
		else if( ispunct(c))
		{
			letter = c;
			tail = init_symbol(inputFilePointer, tail, c);
			c = fgetc(inputFilePointer);
		}

		else
		{
			if(clean == 1 || source == 1)
			{
				printf("%c", c);
			}
			c = fgetc(inputFilePointer);
		}
	}

	if(clean == 1)
	{
		printf("\n");
	}

	FILE *inputForSource = fopen("file.txt", "r");
	if(source == 1)
	{
		printf("source code\n");
		printf("---------------\n");
		int d;
		while((d = getc(inputForSource)) != EOF)
		{
			putchar(d);
		}
	}



	fclose(inputFilePointer);
	fclose(inputForSource);

	if(clean == 1 || source == 1)
	{
		printf("\n");
	}
	printf("tokens:\n");
	printf("--------------\n");

	for( ; head->next != NULL; head = head->next){
		printf("%s\t\t", head->word);
		findLexeme(head->word,&symbol_int);
	}

	return 0;
}

node* starts_with_letter( FILE* inputFile, node* tail, char firstLetter)
{
	int wordLength  = 12, nextLetter = 0, letterPosition = 1;

	char* word = calloc(wordLength, sizeof(char) );


	strcpy(word, "");
	word[0] = firstLetter;


	nextLetter = fgetc(inputFile);


	while( isalpha(nextLetter) || isdigit(nextLetter) )
	{
		if( letterPosition >= wordLength )
		{
			wordLength = wordLength * 2;
			word = realloc(word, wordLength + 1);
		}

		word[letterPosition] = nextLetter;
		letterPosition++;
		nextLetter = fgetc(inputFile);
	}

	tail->word = malloc( strlen(word) + 1 );
	strcpy( tail->word, word);
	tail->next = createNode();

	free(word);

	if( nextLetter != EOF)
	fseek(inputFile, -1, SEEK_CUR);

	if(clean == 1 || source == 1)
	{
		printf("%s", tail->word );
	}

	return tail->next;

}


node* starts_with_integer( FILE* inputFile, node* tail, char firstDigit)
{
	/*Comment to be ignored*/
	int numDigits = 5, nextDigit = 0, digitPosition = 1;

	char* word = calloc(numDigits + 1, sizeof(char) );
	strcpy( word, "");
	word[0] = firstDigit;

	nextDigit = fgetc(inputFile);

	while( isdigit( nextDigit) )
	{
		if(digitPosition >= numDigits)
		{
			numDigits = numDigits * 2;
			word = realloc( word, numDigits + 1);
		}

		word[digitPosition] = nextDigit;
		digitPosition++;
		nextDigit = fgetc(inputFile);
	}

	if( isalpha(nextDigit) )
	{
		printf("Error! Identifier needs to be only letters \n");
		exit(1);
	}

	tail->word = malloc( strlen(word) + 1);
	strcpy( tail->word, word);
	tail->next = createNode();

	if( nextDigit != EOF)
	fseek(inputFile, -1, SEEK_CUR);

	free(word);

	if(clean == 1 || source == 1)
	{
		printf("%s", tail->word);
	}

	return tail->next;

}

node* createNode()
{
	node* ptr = malloc(sizeof(node));

	ptr->next = NULL;

	return ptr;
}

void findLexeme(char *text, int *numSymbs )
{

	if ( isalpha(text[0]) )
	{

		if (strlen(text) > 11)
		{
			printf("Error! Identifier too long \n" );
			return;
		}

		if (strcmp(text, "odd") == 0){
			printf("%d\n",8);
		}
		else if (strcmp(text, "begin") == 0){
			printf("%d\n",21);
		}
		else if (strcmp(text, "end") == 0){
			printf("%d\n", 22 );
		}
		else if (strcmp(text, "if") == 0){
			printf("%d\n", 23 );
		}
		else if (strcmp(text, "then") == 0){
			printf("%d\n", 24 );
		}
		else if (strcmp(text, "while") == 0){
			printf("%d\n", 25 );
		}
		else if (strcmp(text, "if") == 0){
			printf("%d\n", 26 );
		}
		else if (strcmp(text, "then") == 0){
			printf("%d\n", 27 );
		}
		else if (strcmp(text, "const") == 0){
			printf("%d\n", 28 );
		}
		else if (strcmp(text, "var") == 0){
			printf("%d\n", 29 );
		}
		else if (strcmp(text, "procedure") == 0){
			printf("%d\n", 30 );
		}
		else if (strcmp(text, "write") == 0){
			printf("%d\n", 31 );
		}
		else if (strcmp(text, "read") == 0){
			printf("%d\n", 32 );
		}
		else if (strcmp(text, "else") == 0){
			printf("%d\n", 33 );
		}
		else{
			printf("2\n");
		}
	}

	else if ( ispunct(text[0]) )
	{
		if ( strcmp( text, "+") == 0 ) {
			printf("%d\n", 4);
		}
		else if ( strcmp( text, "-") == 0 ) {
			printf("%d\n", 5);
		}
		else if ( strcmp( text, "*") == 0 ) {
			printf("%d\n", 6);
		}
		else if ( strcmp( text, "/") == 0 ) {
			printf("%d\n", 7);
		}
		else if ( strcmp( text, "(") == 0 ) {
			printf("%d\n", 15);
		}
		else if ( strcmp( text, ")") == 0 ) {
			printf("%d\n", 16);
		}
		else if ( strcmp( text, "=") == 0 ) {
			printf("%d\n", 9);
		}
		else if ( strcmp( text, ",") == 0 ) {
			printf("%d\n", 17);
		}
		else if ( strcmp( text, ".") == 0 ) {
			printf("%d\n", 19);
		}
		else if(strcmp(text,"<") == 0){
			printf("%d\n", 11);
		}
		else if ( strcmp(text, "<>") == 0 ){
			printf("%d\n", 10);
		}
		else if ( strcmp(text, "<=") == 0 ){
			printf("%d\n", 12);
		}
		else if ( strcmp(text, ">=") == 0 ){
			printf("%d\n", 14);
		}else if ( strcmp(text, ">") == 0 ){
			printf("%d\n", 13);
		}
		else if ( strcmp(text, ";") == 0 ){
			printf("%d\n", 18);
		}
		else if ( strcmp(text, ":=") == 0 ){
			printf("%d\n", 20);
		}

	}

	else if ( isdigit(text[0]) )
	{
		if (strlen( text ) > 5 )
		{
			printf("Error! Number greater than 2^16-1. \n" );
			return;
		}
		printf("3\n");
	}

}

node* init_symbol( FILE* inputFile, node* tail, char firstSymbol)
{
	int max_symbol = 2;

	char *symbol = calloc(max_symbol + 1, sizeof(char) );

	strcpy( symbol, "");
	symbol[0]= firstSymbol;

	switch( firstSymbol)
	{
		case '/':
		{
			char nextChar;

			nextChar = fgetc(inputFile);

			if( nextChar == '*' )
			{
				nextChar = fgetc(inputFile);

				do
				{
					while (nextChar != '*')
					{
						if( nextChar == EOF )
						{
							printf("Error: Comments do not have closing brace \n");
							exit(0);
						}
						nextChar = fgetc(inputFile);

					}
					nextChar = fgetc(inputFile);

				}while(nextChar != '/');


				return tail;
			}
			else
			{
				if( nextChar != EOF )
				{
					fseek(inputFile, -1, SEEK_CUR);
				}
				break;
			}
		}

		case '>':
		{
			char nextChar;

			int digitPos = 1;

			nextChar = fgetc(inputFile);

			if( nextChar == '=')
			{
				symbol[digitPos] = nextChar;
			}
			else
			{
				if ( nextChar != EOF )
				{
					fseek(inputFile, -1, SEEK_CUR);
				}
				break;
			}
		}

		case '<' :
		{
			char nextChar;

			int digitPos = 1;

			nextChar = fgetc(inputFile);

			if( nextChar == '=' || nextChar == '>')
			{
				symbol[digitPos] = nextChar;
			}
			else
			{
				if( nextChar != EOF )
				{
					fseek( inputFile, -1, SEEK_CUR);
				}
				break;
			}
		}

		case ':' :
		{
			char nextChar;

			int digitPos = 1;

			nextChar = fgetc(inputFile);

			if( nextChar == '=')
			{
				symbol[digitPos] = nextChar;
			}
			else
			{
				printf("Error! Invalid token\n");
				exit(1);
			}
			break;
		}
												// list of special symbols
		case '+' :
		case '-' :
		case '*' :
		case '(' :
		case ')' :
		case '=' :
		case ',' :
		case '.' :
		case ';' :
		break;
		default :
		printf("Error! Invalid token \n");
		exit(1);
	}

	tail->word = malloc( strlen(symbol) + 1 );
	strcpy( tail->word, symbol);
	tail->next = createNode();

	free(symbol);
	if(clean == 1 || source == 1)
	{
		printf("%s", tail->word );
	}

	return tail->next;

}
